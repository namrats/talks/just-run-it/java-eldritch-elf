# Java the Eldritch Elf

Java the Eldritch Elf, or JEE for short, is a lightning quick, hyper-agile, super app.

[![(c) William RM Bartlett, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [LICENSE.md](LICENSE.md)

## Build

Requirements:

- A shell (ash, bash, dash, fish, ksh, ... or zsh)
- Docker 18.09.3

On your favorite shell, run the following command.

```shell
./mvnw package
```

Alternatively, if you are using zsh with [Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh)
and the `mvn` plugin is active, you can simply run `mvn package`.

If all goes well, a subdirectory named `target` will appear with an executable JAR file inside.

## Run

Since the artifact produced is already executable, the following command will
start the service.

```shell
target/java-eldritch-elf*.jar
```

By default, the service listens to port 8080. This can be configured from the
command line.

```shell
target/java-eldritch-elf*.jar --server.port 8081
```

## Use

The service is just a static web page accessible at http://localhost:8080/.