package net.namrats.jri.javaeldritchelf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaEldritchElfApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaEldritchElfApplication.class, args);
	}

}
